# txt2tree

Une petite librairie permettant de regénérer la structure d'un sommaine (Table of Content) à partir du contenu brut (UFT8) d'un document texte au format TXT.


## Exemple 
Voici un extrait du contenu brut du fichier `data/CCTP_menuiserie_1.txt` :
![image](wiki/cctp_1_extract_1.png)

Et le résultat associé 

```
TABLE OF CONTENT (data/CCTP_menuiserie_1.txt)
├── 1. GENERALITES
│   ├── 1.1 Prescriptions générales
│   ├── 1.2 Documents de référence
│   │   ├── 1.2.1  Liste des DTU :
│   │   └── 1.2.2  Règles de calculs :
│   ├── 1.3 Obligation de l'entrepreneur
│   │   ├── 1.3.1  LIAISON AVEC LES AUTRES CORPS D'ETAT
│   │   ├── 1.3.2  ENTRETIEN DES OUVRAGES
│   │   ├── 1.3.3  TRAVAUX ANNEXES
│   │   └── 1.3.4  PRINCIPE DE L'ETUDE
│   ├── 1.4 Prescriptions particulières
│   │   ├── 1.4.1  Organigramme
│   │   ├── 1.4.2  Matériaux
│   │   ├── 1.4.3  TAUX D'HUMIDITE
│   │   ├── 1.4.4  CONTREPLAQUE ET PANNEAUX DE PARTICULES
│   │   ├── 1.4.5  IMPRESSION DES BOIS
│   │   ├── 1.4.6  QUINCAILLERIE
│   │   └── 1.4.7  Mise en oeuvre
│   ├── 1.5 Traitements
│   │   ├── 1.5.1  TRAITEMENT AUX INSECTES
│   │   ├── 1.5.2    TRAITEMENT AU FEU
│   │   ├── 1.5.3    TRAITEMENT ACOUSTIQUE
│   │   └── 1.5.4  Prise - Réservations - Scellement
│   ├── 1.6 Dossier d'exécution
│   │   ├── 1.6.1  Plans d'exécution :
│   │   ├── 1.6.2  Visa du dossier d'exécution :
│   │   └── 1.6.3  Notes de calculs :
│   └── 1.7 Dossier des ouvrages exécutés
├── 2. MENUISERIES EXTERIEURES BOIS
│   ├── 2.1 MENUISERIES EXISTANTES
│   │   ├── 2.1.1  Dépose
│   │   ├── 2.1.2  Adaptation
│   │   └── 2.1.3  Remplacement de vitrage
│   ├── 2.2 MENUISERIES EXTERIEURES
│   │   ├── 2.2.1  Quincaillerie :
│   │   └── 2.2.2  Vitrage pour châssis extérieurs:
│   ├── 2.3 FENETRES
│   │   ├── 2.3.1  Ouvrant coulissant
│   │   ├── 2.3.2  Ouvrant oscillo-battant
│   │   ├── 2.3.3  Ouvrant à la française
│   │   ├── 2.3.4  Ouvrant en abattant
│   │   ├── 2.3.5  Ouvrant basculant
│   │   └── 2.3.6  Ouvrant à soufflet
│   ├── 2.4 PORTE en profilé aluminium
│   ├── 2.5 POSE EN RENOVATION de MENUISERIES extérieures bois
│   ├── 2.6 PORTES
│   ├── 2.7 VITRAGES SPECIAUX
│   │   ├── 2.7.1
│   │   ├── 2.7.2
│   │   ├── 2.7.3
│   │   └── 2.7.4
│   ├── 2.8 FERMETURES ELECTRIQUES
│   │   ├── 2.8.1 Ventouse électrique
│   │   ├── 2.8.2
│   │   ├── 2.8.3
│   │   └── 2.8.4
│   ├── 2.9 ACCESSOIRES
│   │   ├── 2.9.1 Barres de seuils
│   │   ├── 2.9.2
│   │   └── 2.9.3
│   └── 2.10 AMENAGEMENTS EXTERIEURS
│       └── 2.10.1 Claustra bois
├── 3. MENUISERIES INTERIEURES BOIS
│   ├── 3.1 TRAVAUX DE DEPOSE
│   │   ├── 3.1.1  Dépose des revêtements de sol stratifié
│   │   ├── 3.1.2 Dépose du parquet bois
│   │   ├── 3.1.3
│   │   ├── 3.1.4
│   │   ├── 3.1.5
│   │   └── 3.1.6
│   ├── 3.2 TRAVAUX DE REVISION
│   │   └── 3.2.1 Révision des menuiseries intérieures (portes ou placard)
│   ├── 3.3 GESTION DES CLES
│   ├── 3.4 Organigramme
│   ├── 3.5 Gestion des clés du chantier
│   ├── 3.6 INSTALLATION DE CHANTIER
│   │   ├── 3.6.1 Cloisons provisoires
│   │   └── 3.6.2 Porte provisoire de fermeture
│   ├── 3.7 REMPLACEMENT DE PORTE
│   │   ├── 3.7.1  VANTAIL  ou VANTAUX :
│   │   ├── 3.7.2  FERRAGE par :
│   │   ├── 3.7.3  FERME-PORTE (FP) à glissière à frein hydraulique type DORMA TS 93 de chez DORMA ou équivalent,
│   │   ├── 3.7.4  Porte BC + PEINT
│   │   ├── 3.7.5  Porte BCCD + PEINT
│   │   ├── 3.7.6  Porte SS + PEINT
│   │   ├── 3.7.7  Porte BMI + PEINT
│   │   ├── 3.7.8  Plus-value finition stratifié 2 faces STRAT
│   │   ├── 3.7.9  Plus-value acoustique 30dB
│   │   ├── 3.7.10  Plus-value CF1/2H + FP
│   │   ├── 3.7.11  Plus-value CF1H + FP
│   │   ├── 3.7.12  Plus-value ferme-porte
│   │   └── 3.7.13
│   └── 3.8 BLOC-PORTE AME PLEINE
│       ├── 3.8.1  HUISSERIE à recouvrement ou affleurant :
│       ├── 3.8.2  VANTAIL  ou VANTAUX :
│       ├── 3.8.3  FERME-PORTE (FP) à glissière à frein hydraulique type DORMA TS 93 de chez DORMA ou équivalent,
│       ├── 3.8.4  Bloc-porte BC + PEINT
│       ├── 3.8.5  Bloc-porte BCCD + PEINT
│       ├── 3.8.6  Bloc-porte SS + PEINT
│       ├── 3.8.7  Plus-value finition stratifié 2 faces STRAT
│       ├── 3.8.8  Plus-value acoustique 30dB
│       ├── 3.8.9  Plus-value CF1/2H + FP
│       ├── 3.8.10  Plus-value CF1H + FP
│       ├── 3.8.11  Plus-value ferme-porte
│       └── 4.1.1  Faux-plafond Hunter Douglas Ouvert
├── 4. FAUX-PLAFOND BOIS
└── 5. AMENAGEMENTS INTERIEURS
    ├── 5.1 Habillages
    │   ├── 5.1.1 Habillage bloc-porte existant
    │   ├── 5.1.2
    │   ├── 5.1.3
    │   ├── 5.1.4
    │   ├── 5.1.5
    │   ├── 5.1.6  Etagères - Ep. 19mm
    │   ├── 5.1.7  Tablettes - Ep. 30mm
    │   └── 5.1.8  Plan de travail - Ep. 38mm
    ├── 5.2 Escalier
    ├── 5.3 Divers
    ├── 5.4 Pose d'affichages (fourniture hors lot)
    ├── 5.5 Pose écran de projection (fourniture hors lot)
    ├── 5.6 PLACARDS ET AMENAGEMENT
    │   ├── 5.6.1  Remplacement portes de placard pivotantes - Conservation du dormant
    │   ├── 5.6.2  Remplacement portes de placard coulissante - Conservation des rails
    │   ├── 5.6.3  Placard portes pivotantes
    │   ├── 5.6.4  Placard portes coulissantes
    │   └── 5.6.5  Aménagement
    ├── 5.7 DIVERS
    ├── 5.8 Trappes de visite
    ├── 5.9 Plinthes
    │   ├── 5.9.1  Plinthes médium à peindre
    │   ├── 5.9.2  Plinthe aluminium anodisé
    │   ├── 5.9.3  Plinthe aluminium laqué
    │   ├── 5.9.4  Plinthe décor CREATION 70 de chez GERFLOR
    │   └── 5.9.5  Reprise plinthes stylobate existantes
    ├── 5.10 Cimaise
    └── 5.11 Caisson store
```