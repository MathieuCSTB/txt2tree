CommunautéBdeBcommunesBVézèreB
MonédièresBMillesources
15BAvenueBduBGénéralBdeBGaulle
19268BTREIGNAC
ConstructionBd-uneBmaisonBdeBsantéBàB
TREIGNAC
	                                                                      Décembre 2017            
LotBN°85BMENUISERIESBINTERIEURESBBOIS
CB6BCB6BTB6BPB6





 

!"###$
$%&#'(((!#"
)%&*+)%,-
+
./$


01
2	/
3
!"!##/0410
$%&#'''55675
)%&.)+8
 -9+:

/$;%
)< 
.<0.=

!(3/%>
!"###$
$%&#'''2##"!
)%&-%!"+:

)
/$0
'!3
/

!"###$
$%&#'''26("2
)%&%)%%-9+:


	
?)=@$01	
A#'000$0/0
	0$0$	0B
DEFINITIONBDEBLABPRESTATIONDEFINITIONBDEBLABPRESTATION
Le CCTP  du présent lot concerne les travaux du Lot N°05 MENUISERIES INTERIEURES BOIS à exécuter 
pour la Construction d'une maison de santé à TREIGNAC pour le compte de la Communauté de communes 
Vézère Monédières Millesources à TREIGNAC (19-CORREZE).
DOCUMENTSBTECHNIQUES7NORMESDOCUMENTSBTECHNIQUES7NORMES
Les  entreprises  seront  soumises  pour  l'exécution  de  leurs  travaux,  aux  clauses  et  spécifications  des 
documents ci-après, sachant que cette liste n'est pas limitative :
DTU 31.1 : Charpente et escalier en bois
DTU 36.1 : Menuiseries en bois
DTU 37.1 : Menuiseries métalliques
DTU 39 : Miroiterie-Vitrerie
Elles  seront  soumises  en  outre,  aux  prescriptions  des  textes  réglementaires  en  vigueur  au  moment  de 
l'exécution des travaux, notamment :
Règles NV 65-N84 définissant les effets de la neige et du vent sur les constructions 
Règles N 84 définissant les effets de la neige sur les constructions
Aux normes AFNOR qualitatives et dimensionnelles.
Toutes normes et DTU en vigueur à la remise des offres.
ANNEXESANNEXES
A/ Note commune
Les entrepreneurs devront :
Vérifier tous les ouvrages et quantités portés au devis quantitatif, chacun en ce qui le concerne, compléter 
éventuellement ce document pour obtenir une parfaite exécution des travaux qui seront traités à prix global et 
forfaitaire, sans aucune majoration, y compris pour difficultés d'exécution.
NOTE IMPORTANTE
Chaque entreprise devra vérifier les quantités fournies au dossier de consultation et ne pourra les contester 
après signature de son marché.
Les éventuelles modifications de quantités seront bien spécifiées "Positionner la nouvelle quantité sous celle 
fournie à la consultation ".
Le mode de métré et le sous détail pourront être fournis à l'entreprise adjudicataire suivant les principes de la 
circulaire du 9 mars 1982.
B/Montant de l'offre
Pour chiffrer sa proposition, l'entreprise devra disposer du dossier comportant les éléments suivants :
-Un dossier de plans complet
-Un CCTP (Cahier des Clauses Techniques Particulières) tous corps d'état ou par lot
-Un DPGF (Décomposition du Prix Global et Forfaitaire) de son lot
-Un CCAP (Cahier des Clauses Administratives Particulières)
-Un RC (Règlement de la Consultation)
-Un AE (Acte d'Engagement)
C/Fin de chantier
A  l'issue  des  travaux,  l'entrepreneur  fournira  au  Maître  d'ouvrage  (en  3  exemplaires)  un  dossier  complet 
comprenant :
-Modifications apportées au dossier
-Fiches techniques des matériaux employés
-Fiches d'entretien des matériaux employés certifiés par le fabricant.
QUALITEBDESBMATERIAUX7MISEBENBOEUVREQUALITEBDESBMATERIAUX7MISEBENBOEUVRE
Généralités
L'entreprise titulaire du présent lot devra fournir tous les échantillons nécessaires lors de la première réunion 
de chantier où elle sera convoquée afin que l'architecte puisse entériner ses choix. Les matériaux utilisés 
seront de première qualité et seront conformes aux spécifications des Normes NF, dans tous les cas, les 
provenances (non spécifiées dans le présent CCTP) seront soumises à l'agrément de l'architecte.
.#'+!)<	80
	
?)=@$01	
A#'000$0/0
	0$0$	0B
Il  est  rappelé  que  la  notion  de  similitude  (techniquement  équivalent)  appartient  à  l'architecte.  En  cas  de 
divergence  de  vue  avec  l'entrepreneur,  celui-ci  sera  tenu  de  mettre  en  oeuvre  le  matériau  indiqué  en 
référence au présent CCTP.
L'implantation des cloisons sera réalisée par le platrier avant la pose des huisseries.
Le recours aux produits et matériaux apportant une plus value environnementale est favorisé. Les bois utilisés 
devront justifier des labels FSC ou PEFC.
Manutention7Protection
Toutes  les  opérations,  frais  et  faux  frais  relatifs  aux  transports,  manoeuvres  et  manutentions  diverses,  et 
durant les travaux des autres corps d'état, traçage, pose, réglage, ajustages, mise en état de fonctionnement, 
etc... concernant les ouvrages du présent lot, sont à la charge de l'entrepreneur.
Celui-ci  sera  entièrement  responsable  de  ses  ouvrages  jusqu'à  la  réception,  devra  prendre  toutes  les 
précautions pour que les éléments ne soient pas détériorés compte tenu des aléas du chantier.
.#'+)<	80
Construction d'une maison de santé à TREIGNAC
Lot N°05 MENUISERIES INTERIEURES BOIS
DESCRIPTIONS DES OUVRAGES
Sommaire
05.1  BLOC-PORTE1
05.1.1  Dépose1
05.1.2  Révision1
05.1.3  Ame pleine1
05.1.4  Coupe-feu3
05.1.5  Acoustique - Coupe feu3
05.1.6  Anti-rayons X4
05.1.7  Coulissante4
05.1.8  Accessoires5
05.2  PLACARD5
05.2.1  Portes5
05.2.2  Aménagement6
05.3  MOBILIER6
05.3.1  Meubles6
05.4  DIVERS6
05.4.1  Platelage6
05.4.2  Trappes7
05.4.3  Cimaise7
05.4.4  Organigramme7
05.4.5  Protection/Nettoyage8
Page 05.0Economiste  - Cabinet JP DELOMENIE SARL
Construction d'une maison de santé à TREIGNAC
Lot N°05 MENUISERIES INTERIEURES BOIS
DESCRIPTIONS DES OUVRAGES
BLOC-PORTE05.1BLOC-PORTE
Dépose05.1.1
Portes05.1.1.1
Dépose de bloc-porte05.1.1.1 1L'entreprise devra la dépose trés soigneuse de bloc-portes existants, compris ouvrages attenants, raccords et 
rebouchages nécessaires, évacuation et toutes sujétions nécessaires à une parfaite et complète réalisation.
Localisation :

Pour dépose du bloc-porte sur ancien local radio du bâtiment existant, suivant plans et indications de 
l'architecte.
Révision
05.1.2
Remise en état de portes existantes05.1.2.1
Révision de blocs-portes conservés05.1.2.1 1Révision de bloc-porte existant comprenant :°Remplacement à l'identique tous les éléments défectueux
°Rabotage, ajustage, graissage
°Dépose et remplacement de la serrure existante par une serrure de conception identique mais avec profil 
pour recevoir un cylindre européen sur organigramme, compris toutes adaptations nécessaires
°Dépose de la garniture existante et remplacement par un ensemble  béquilles sur plaques en inox des Ets 
NORMBAU série Est ou techniquement équivalent
°Mise en place d'un arrêt de porte assorti
L'entreprise devra toutes les sujétions nécessaires à un fonctionnement normal.
Localisation :
Pour révision de la porte du local électrique dans accueil-dégagement à l'intérieur du bâtiment existant.
Ame pleine
05.1.3
Ouvrante/"Gaines"05.1.3.1
Pleine - de 73/204 -05.1.3.1 1Bloc-porte des BLOCFER ou techniquement équivalent composé de :
PORTE:          
-Ame pleine de 40mm d'épaisseur dans cadre sapin.
FINITION:
-Fibres dures à peindre
HUISSERIE:  
-Sapin à feuillure compris paumelles
°En applique entre cloisons
QUINCAILLERIE:
-Serrure à larder des Ets YALE série Soprano ou techniquement équivalent pour :
°Bec de cane avec triangle de 11 mm modèle EDF
GARNITURE:
-Rosaces en PVC des Ets NORMBAU ou techniquement équivalent
L'entreprise  devra  toutes  les  sujétions  nécessaires  de  mise  en  oeuvre  conformément  aux  prescriptions 
techniques des fabricants, DTU et normes en vigueur.
DIMENSIONS : 73 x 204 ht
Localisation :
Pour porte de gaine dans zone d'attente dans le bâtiment en extension, suivant plans et indications de 
l'architecte.
Page 05.1Economiste  - Cabinet JP DELOMENIE SARL
Construction d'une maison de santé à TREIGNAC
Lot N°05 MENUISERIES INTERIEURES BOIS
DESCRIPTIONS DES OUVRAGES
Ouvrante à 1 vantail05.1.3.2
Pleine - de 83/204 -05.1.3.2 1Bloc-porte des Ets BLOCFER ou techniquement équivalent composé de :
PORTE:
-Ame pleine de 40mm d'épaisseur
FINITION:
-Fibres dures à peindre
HUISSERIE:  
-Sapin à feuillure compris paumelles en nombre suffisant et baguettes d'habillage nécessaires
°A recouvrement des cloisons jusqu'à 110mm d'épaisseur.
°En applique dans ébrasements de cloisons au-delà de 110mm d'épaisseur.
°En applique dans ébrasements de murs (béton ou aggloméré)
QUINCAILLERIE:
-Serrure à mortaiser des Ets BRICARD série Robust ou techniquement équivalent pour :
°Bec de cane (type A)
°Bec de cane à condamnation (type B)
°Sureté à pène dormant pour cylindre européen sur organigramme (type C)
GARNITURE:
-Ensemble béquilles sur plaques en inox des Ets NORMBAU série Est ou techniquement équivalent
BUTOIR:
-Arrêt de porte assorti
L'entreprise devra toutes les sujétions nécessaires de mise en oeuvre conformément aux prescriptions 
techniques des fabricants, DTU et normes en vigueur.
DIMENSIONS : 83 x 204 cm ht
Localisation :
Pour porte sur WC du bâtiment extension (type B).Pour porte sur atelier podologue du bâtiment en extension (type C).
Pleine - de 93/204 - barre de tirage PMR
05.1.3.2 2Bloc-porte des Ets BLOCFER ou techniquement équivalent composé de :
PORTE:
-Ame pleine de 40mm d'épaisseur
FINITION:
-Fibres dures à peindre
HUISSERIE:  
-Sapin à feuillure compris paumelles
°A recouvrement des cloisons jusqu'à 110mm d'épaisseur.
°En applique dans ébrasements de cloisons au-delà de 110mm d'épaisseur.
°En applique dans ébrasements de murs (béton ou aggloméré)
QUINCAILLERIE:
-Serrure à mortaiser des Ets BRICARD série Robust ou techniquement équivalent pour :
°Bec de cane (type A)
°Bec de cane à condamnation (type B)
°Sureté à pène dormant pour cylindre européen sur organigramme (type C)
GARNITURE:
-Ensemble béquilles sur plaques en inox des Ets NORMBAU série Est ou techniquement équivalent
BUTOIR:
-Arrêt de porte assorti
ACCESSOIRES:
-Barre de tirage PMR assortie à la quincaillerie
L'entreprise  devra  toutes  les  sujétions  nécessaires  de  mise  en  oeuvre  conformément  aux  prescriptions 
techniques des fabricants, DTU et normes en vigueur.
DIMENSIONS : 93 x 204 cm ht avec barre de tirage PMR
Localisation :
Pour porte sur WC du bâtiment en existant (type B).
Page 05.2Economiste  - Cabinet JP DELOMENIE SARL
Construction d'une maison de santé à TREIGNAC
Lot N°05 MENUISERIES INTERIEURES BOIS
DESCRIPTIONS DES OUVRAGES
Coupe-feu05.1.4
Ouvrante à 1 vantail05.1.4.1
Coupe-feu 1/2 H - de 83/204 + FP05.1.4.1 1Blocs-portes  CF  à  âme  pleine  isoplane  type  "Blocofeu"  des  Ets  BLOCFER  ou  techniquement  équivalent 
composé de :
PORTE:          
-Ame en panneau de particule de 40mm d'épaisseur dans cadre bois exotique avec joints.
Tenue au feu: CF 1/2 Heure.
FINITION:
-Fibres dures à peindre
HUISSERIE:  
-Bois exotique à feuillure compris paumelles et baguettes d'habillage
°A recouvrement des cloisons jusqu'à 110mm d'épaisseur.
°En applique dans ébrasements de cloisons au-delà de 110mm d'épaisseur.
°En applique dans ébrasements de murs (béton ou aggloméré)
QUINCAILLERIE:
-Serrure à mortaiser des Ets BRICARD série Robust ou techniquement équivalent pour :
°Bec de cane (type A)
°Bec de cane à condamnation (type B)
°Sureté à pène dormant pour cylindre européen sur organigramme (type C)
GARNITURE:
-Ensemble béquilles sur plaques en inox des Ets NORMBAU série Est ou techniquement équivalent
 BUTOIR:
-Arrêt de porte assorti
FERME-PORTE:
-A glissière des Ets BRICARD série 605 ou techniquement équivalent, adapté à chaque vantail.
TENUE AU FEU:
-Tenue au feu du bloc-porte et de ces accessoires: CF 1/2 Heure
L'entreprise  devra  toutes  les  sujétions  nécessaires  de  mise  en  oeuvre  conformément  aux  prescriptions 
techniques des fabricants, DTU et normes en vigueur.
DIMENSIONS : 83 x 204 ht avec ferme porte
Localisation :
Pour porte sur local dasri du bâtiment en extension et sur local technique du bâtiment existant (type C).
Acoustique - Coupe feu
05.1.5
Ouvrante à 1 vantail05.1.5.1
Rw=38 dB - CF/PF 1/2 H - de 93/204 + FP05.1.5.1 1Bloc-porte acoustique à 1 vantail des Ets MALERBA série Isophone ou techniquement équivalent composé de 
:
PORTE:          
-Ame composite accoustique de 40mm d'épaisseur dans cadre bois exotique avec joint balai ajustable en 
partie basse.
Tenue au feu : PF et CF 1/2 Heure.
Affaiblissement acoustique : Rw = 38 dB.
FINITION:
-Fibres dures à peindre
HUISSERIE:  
-Bois exotique à feuillure avec joints 2 lèvres compris paumelles de 140
°A recouvrement des cloisons jusqu'à 110mm d'épaisseur.
°En applique dans ébrasements de cloisons au-delà de 110mm d'épaisseur.
°En applique dans ébrasements de murs (béton ou aggloméré)
QUINCAILLERIE:
-Serrure à mortaiser des Ets BRICARD série Robust ou techniquement équivalent pour :
°Bec de cane (type A)
°Bec de cane à condamnation (type B)
°Sureté à pène dormant pour cylindre européen sur organigramme (type C)
GARNITURE:
-Ensemble béquilles sur plaques en inox des Ets NORMBAU série Est ou techniquement équivalent
FERME-PORTE:
Page 05.3Economiste  - Cabinet JP DELOMENIE SARL
Construction d'une maison de santé à TREIGNAC
Lot N°05 MENUISERIES INTERIEURES BOIS
DESCRIPTIONS DES OUVRAGES
-A glissière des Ets BRICARD série 397 ou techniquement équivalent, adapté à chaque vantail.
BUTOIR:
-Arrêt de porte.
L'entreprise  devra  toutes  les  sujétions  nécessaires  de  mise  en  oeuvre  conformément  aux  prescriptions 
techniques des fabricants, DTU et normes en vigueur.
DIMENSIONS : 93 x 204 ht avec ferme porte
Localisation :
Pour portes sur cabinet cabinet infirmiers, salle de réunion, dentiste, cabinet dentiste 1 et 2, cabinet 
kinésithérapeute, podologue des bâtiments en extension et sur bureaux médicaux 1 et 2 et gynécologue dans 
bâtiment existant et pour porte entre zone d'attente et secrétariat kiné dans le bâtiment existant (type C).
Anti-rayons X
05.1.6
Ouvrante à 1 vantail05.1.6.1
BP anti-rayons X - de 93/20405.1.6.1 1Bloc-porte  d'intérieur  Anti  rayons  X,  des Ets BLOCFER série BLOCOPLOMB ou techniquement 
équivalent composé de :
PORTE:          
-Ame en panneau de particule de 40mm d'épaisseur avec raidisseur dans cadre bois exotique avec 
sous-parement 2 feuilles de plomb 10/10.
Tenue au feu: PF et CF 1/2 Heure.
FINITION:
-Fibres dures à peindre
HUISSERIE:  
-Métal pour cloison maçonnée, avec peinture époxy par cataphorèse, acier 15/10 + talon de 10 mm 
comprenant :
Paumelles nécessaires
Joints intumescents
Feuille de plomb 20/10e
°A recouvrement des cloisons jusqu'à 110mm d'épaisseur.
°En applique dans ébrasements de cloisons au-delà de 110mm d'épaisseur.
°En applique dans ébrasements de murs (béton ou aggloméré)
QUINCAILLERIE:
-Serrure à mortaiser des Ets BRICARD série Robust ou techniquement équivalent pour :
°Bec de cane (type A)
°Bec de cane à condamnation (type B)
°Sureté à pène dormant pour cylindre européen sur organigramme (type C)
GARNITURE:
-Ensemble béquilles sur plaques en inox des Ets NORMBAU série Est ou techniquement équivalent
BUTOIR:
-Arrêt de porte assorti.
L'entreprise  devra  toutes  les  sujétions  nécessaires  de  mise  en  oeuvre  conformément  aux 
prescriptions techniques des fabricants, DTU et normes en vigueur.
DIMENSIONS : 93 x 204 cm ht
Localisation :

Pour porte sur local panoramique dans dentiste du bâtiment en extension (type C).
Coulissante
05.1.7
Escamotable05.1.7.1
Porte coulissante escamotable de 93/20405.1.7.1 1Bloc-porte coulissant à 1 vantail des Ets BLOCFER et ECLISSE ou techniquement équivalent composé de :
PORTE:          
-Porte à ame en panneau de particule de 40 mm d'épaisseur dans cadre sapin - Finition : Fibres dures à 
Page 05.4Economiste  - Cabinet JP DELOMENIE SARL
Construction d'une maison de santé à TREIGNAC
Lot N°05 MENUISERIES INTERIEURES BOIS
DESCRIPTIONS DES OUVRAGES
peindre
CHASSIS/HUISSERIE:
-Châssis  métallique  pour  porte  simple  des  Ets  ECLISSE  type  Unique  100  complet  ou  techniquement 
équivalent, pour cloison en plaques de plâtre de 100 mm, compris habillage bois, couvre-joint, joints, etc ...
-Habillage sur chaque face en plaques de plâtre BA 13, compris traitement des joints
QUINCAILLERIE:
-Serrure à larder des Ets ECLISSE type Kit 2 ou techniquement équivalent, avec double bouton et clés
L'entreprise  devra  toutes  les  sujétions  nécessaires  de  mise  en  oeuvre  conformément  aux  prescriptions 
techniques des fabricants, DTU et normes en vigueur.
DIMENSIONS : Suivant plans
Localisation :
Pour portes coulissantes sur les box dans cabinet kinésithérapeute et sur stérilisation dans cabinet dentiste du 
bâtiment en extension.
Accessoires
05.1.8
Serrures/béquillages05.1.8.1
Béquille avec poignée rallongée PMR05.1.8.1 1L'entreprise devra chiffrer en plus-value la fourniture et pose de béquille avec poignée rallongée conforme à la 
réglementation PMR, dans la même gamme que celle prévue dans les articles de blocs-portes compris toutes 
sujétions nécessaires de mise en oeuvre conformément aux reglementations en vigueur.
Localisation :
Pour béquille rallongée sur porte du génycologue dans le bâtiment existant.
PLACARD05.2PLACARD
Portes05.2.1
Coulissantes05.2.1.1
Façade de 190 x 250 ht env05.2.1.1 1Porte coulissante des Ets SOGAL référence "Initial" ou techniquement équivalent comprenant :
- Habillage contre cloisons en contre-plaqué bois ou autres au droit des butées des vantaux.
- Rail de roulement en partie basse
- Guidage en partie haute avec bandeau
- Profilés, bandeau et rails en acier galvanisé prélaqué
- Panneaux en aggloméré mélaminé 2 faces de 10 mm d'épaisseur, avec montants et traverses en profilés 
aluminium assortis
- Boitier porte roulette type "Robotiwn" avec possibilité de réglage de la course
- Etc...
L'entreprise  devra  toutes  les  sujétions  nécessaires  de  mise  en  oeuvre  conformément  aux  prescriptions 
techniques du fabricant, DTU et normes en vigueur.
Dimensions : Suivant plans
Coloris : Au choix de l'architecte.
Localisation :
Pour porte coulissante sur placards dans cabinet kinésithérapeute, cabinet infirmiers et podologue dans le 
bâtiment extension et dans gynécologue, zone d'attente dans bâtiment exixtant suivant plans et indications de 
l'architecte.
Page 05.5Economiste  - Cabinet JP DELOMENIE SARL
Construction d'une maison de santé à TREIGNAC
Lot N°05 MENUISERIES INTERIEURES BOIS
DESCRIPTIONS DES OUVRAGES
Aménagement05.2.2
Rayonnage05.2.2.1
Aggloméré 19 mm mélaminé05.2.2.1 1Aménagement de placard en aggloméré de 19 mm mélaminé blanc toutes faces et chants, posé sur tasseaux 
sapin et composé de :
°1 ou 2  séparations verticales sur toute la hauteur selon la largeur
°5 cours de rayon horizontal espacés de 40 cm de part et d'autre de la séparation
L'entreprise devra toutes les sujétions nécessaires de mise en oeuvre conformément aux DTU et normes en 
vigueur.
Localisation :
Pour aménagements des placards à l'intérieur des locaux des bâtiments en extension et du bâtiment existant, 
suivant plans et indications de l'architecte.
MOBILIER05.3MOBILIER
Meubles05.3.1
Meuble sous évier05.3.1.1
Meuble sous plan de vasque de 120 x 80 x 96 cm ht.05.3.1.1 1Réalisation d'un meuble sous plan de vasque en panneaux hydrofuge des Ets POLYREY ou techniquement 
équivalent comprenant :
-Montants latéraux et intermédiaire en panneaux mélaminés de 19 mm d'épaisseur, alaisés aux 2 faces 
-Plateau bas en panneaux mélaminés de 19 mm d'épaisseur, alaisé aux 2 faces
-Socle en partie basse avec plinthes en retrait, en panneaux mélaminés de 19 mm d'épaisseur
-Habillage de façade arrière en panneaux mélaminés minces
-Rayonnages  intermédiaires  en  panneaux  mélaminés  fixés  entre  montants,  compris  aménagement  pour 
passage des canalisations
-Portes en partie basse en panneaux mélaminés au droit de l'évier, compris tous accessoires nécessaires 
pour ferrage et manoeuvre
-Fixations, assemblage, découpes, etc ...
L'entreprise  devra  toutes  les  sujétions  nécessaires  de  mise  en  oeuvre  conformément  aux  prescriptions 
techniques des fabricants, DTU et normes en vigueur.
-Dimensions : suivant plan de détail
-Coloris : au choix de l'architecte
Nota : Le plan de vasque sera fourni par le plombier.
Localisation :
Pour meuble au droit des plans de vasque dans cabinet infirmiers, cabinet kinésithérapeute, podologue des 
bâtiments en extension et dans gynécologue, bureaux médicaux 1 et 2 du bâtiment existant, suivant plans et 
indications de l'architecte.
DIVERS05.4DIVERS
Platelage05.4.1
En dalle d'aggloméré05.4.1.1
Dalle d'agencement sur solives05.4.1.1 1Réalisation de plancher en dalles d'agencement rainurée-bouvetée en aggloméré de 22 mm CTBH posés sur 
lambourdes en sapin traité I.F.H., fixées sur dalle béton, compris fixations, découpes, habillage des rives et 
toutes sujétions nécessaires de mise en oeuvre conformément aux DTU en normes en vigueur.
Localisation :
Pour platelage en comble du bâtiment existant, suivant demandes du BET Fluides.
Page 05.6Economiste  - Cabinet JP DELOMENIE SARL
Construction d'une maison de santé à TREIGNAC
Lot N°05 MENUISERIES INTERIEURES BOIS
DESCRIPTIONS DES OUVRAGES
Trappes05.4.2
Accés combles/Locaux techniques05.4.2.1
Trappe de 60 x 60 cm - Coupe-feu 1/2 H -05.4.2.1 1Trappe  de  visite  des  Ets  BLOCFER  ou  tecniquement  équivalent,  assurant  un  degré  coupe-feu  de  1/2  h 
composée de :
CHEVETRE:
Chevêtre de dimensions appropriées dans charpente existante
TRAPPE:
-En panneau MDF de 50 mm d'épaisseur avec fermeture par batteuse en acier avec blocage du panneau par 
platines anti-dégondage
CADRE:
-En  bois  exotique  à  recouvrement  des  plaques  de  plâtre,  compris  fixations  et  habillage  périphérique  en 
panneau mince pour retenir l'isolant en comble
FINITION:
-A peindre
L'entreprise  devra  toutes  les  sujétions  nécessaires  de  mise  en  oeuvre  conformément  aux  prescriptions 
techniques du fabricant, DTU et normes en vigueur.
Dimensions : Suivant plans
Localisation :
Pour trappe en plafond du local technique dans le bâtiment existant, suivant demandes du BET Fluides.
Cimaise
05.4.3
Cimaise05.4.3.1
Cimaise en Médium de 22 x 100 mm ht05.4.3.1 1Cimaise en panneaux de fibre de moyenne densité (MDF) type Médium des Ets ISOROY ou techniquement 
équivalent  de  100  mm  hauteur,  compris  coupes,  angles  arrondis,  ponçage,  fixations  et  toutes  sujétions 
nécessaires de mise en oeuvre conformément aux DTU et normes en vigueur.
Epaisseur : 22 mm
Localisation :
Pour cimaise dans acceuil-dégagement et zone d'attente dans les bâtiments en extension et le
bâtiment existant.
Organigramme05.4.4
Cylindre05.4.4.1
Nombre de cylindres05.4.4.1 1L'entreprise devra la fourniture et la pose des cylindres et clés et étudiera sa proposition sur des cylindres 
européens de surêté des Ets BRICARD ou techniquement équivalent.(Clé non reproductible)
L'entreprise du présent lot devra :
-Passe général : (nombre : 1 en 3 exemplaires)
-Passe partiel : (nombre : 3 en 3 exemplaires)
-Clef individuelle : (nombre : 3 par cylindre)
Cet organigramme sera réalisé en présence d'un représentant :
-Du maître d'ouvrage
-De la maîtrise d'oeuvre
-De l'entreprise du lot menuiseries extérieures - serrurerie
-Du fournisseur.
Localisation :
Pour l'ensemble des cylindres équipant les portes intérieures et extérieures des bâtiments en extension et du 
bâtiment existant.
Page 05.7Economiste  - Cabinet JP DELOMENIE SARL
Construction d'une maison de santé à TREIGNAC
Lot N°05 MENUISERIES INTERIEURES BOIS
DESCRIPTIONS DES OUVRAGES
Protection/Nettoyage05.4.5
L'ensemble suivant CCTP.05.4.5 1L'entreprise devra prévoir toutes les protections et échafaudages réglementaires, qui lui seront nécessaires à 
une parfaite et complète sécurité de son personnel, ainsi qu'à une parfaite et complète mise en oeuvre de ses 
produits. De plus elle devra respecter les obligations en matière de coordination pour la santé et la sécurité 
conformément à la loi n° 93-1418 du 31.12.94, ainsi que les conditions prévues au Code du Travail et des 
règlements en vigueur. Elle devra tenir compte des demandes énumérés dans le P.G.C. L'entreprise devra 
également un nettoyage et enlèvement des gravois journalier du chantier ainsi qu'un général à la fin de son 
intervention.
La Maitrise d'Oeuvre se laisse le droit de nommer une entreprise pour réaliser cette prestation si le travail 
réalisé par le titulaire du présent lot ne s'avéré pas satisfaisant. Il va de soi que le montant de ces travaux 
serait imputé sur le marché du présent lot.
Page 05.8Economiste  - Cabinet JP DELOMENIE SARL