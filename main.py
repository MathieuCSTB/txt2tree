import io
import numpy as np
import os
from anytree import Node, RenderTree
import pprint
import re 

class NewlineCharactersMissing(Exception):
    pass


if __name__ == "__main__":

    # Set variables 
    has_table_of_content = False
    has_enumerations = False

    # Name of file 
    filepath = 'data/CCTP_menuiserie_1.txt'

    # Open the TXT document, in UTF-8 encoding format
    with io.open(filepath, encoding='utf8') as f:
        lines = f.readlines()

    # Pre-treatments
    lines_cleaned = []
    lines_length = []
    for line in lines:

        # Remove `\n` character
        tmp = line.replace('\n','')

        # Remove void or useless lines
        tmp = tmp.strip()
        if tmp == '':
            continue

        if tmp.lower() == 'sommaire':
            has_table_of_content = True
        elif tmp.lower() == '- sommaire -':
            has_table_of_content = True

        lines_cleaned.append(tmp)
        lines_length.append(len(tmp))

    # Look for enumerations in start of lines
    for line in lines_cleaned:

        ENUMS = (
            '0 ',
            '0.1 ',
            '1 ',
            '00',
            '00.1 ',
            '01 ',
            '01.2. ',
            '03.0  ',
            '03.1.1',
            '04. ',
            '04.1  ',
            '06.1',
            '07.1 ',
            '1 ',
            '1. ',
            '1 : ',
            '100',
            '1. ',
            '1.1 ',
            '1.01 ',
            '1-1-',
            '1-1.',
            '1.1 - ',
            '1.1- ',
            '1.1)',
            '1 - ',
            '18.1.',
            '2 ',
            '3 ',
            '3.1',
            '4 - ',
            '4.0.',
            '4.1',
            '05.1  ',
            '50   ',
            '501 ',
            '5.1',
            '7 ',
            '7- ',
            '7.1 '
            '7.1- ',
            'ARTICLE 1. ',
            'ARTICLE 1 - ',
            'Article 1 -',
            'ARTICLE 101 - ',
            'CCTP LOT n°',
            'CHAPITRE I - ',
            'Description travaux du lot ',
            'GENERALITES',
            'LOT ',
            'I.1. ',
            'I - ',
            'Lot 1 - ',
            'Lot N°',
            'Lot n°',


        )

        if line.startswith(ENUMS):
            has_enumerations = True

    # Print results
    print(f'has_table_of_content = {has_table_of_content}')
    print(f'has_enumerations = {has_enumerations}')
    print('---------------')

    # Exlude from analyse particular cases
    if np.mean(lines_length) > len(lines_length):
      print('WARNING : your document has lines lengths longer than the total number of lines')

    if np.mean(lines_length) > 2000:
        raise NewlineCharactersMissing(f'Your document `{filepath}` seems to be missing "newline" characters')
    
    # Build potential headers lines list
    list_of_enum = []
    for line in lines_cleaned:
        tmp = bool(re.search("^[0-9]+(\.|\s|-|\))", line))
        if tmp:
            list_of_enum.append(line)

    # Analyse potential headers lines list content
    stock = []
    for i, elt in enumerate(list_of_enum):

        characteristics = {
            'raw_line': None,
            'deepness': 0,
            'number': None,
            'parent': None
        }

        # Case *.*.* (CCTP_menuiserie_1.txt)
        if bool(re.search("^([0-9]+)(\.){0,1}(.*)", elt)):
            characteristics['deepness'] += 1
        if bool(re.search("^([0-9]+)(\.){1}([0-9]+)(\.){0,1}(.*)", elt)):
            characteristics['deepness'] += 1
        if bool(re.search("^([0-9]+)(\.){1}([0-9]+)(\.){1}([0-9]+)(\.){0,1}(.*)", elt)):
            characteristics['deepness'] += 1

        if not bool(re.search("^([0-9]+)(\))(.*)", list_of_enum[0])):

            if bool(re.search("^([0-9]+)(\))(.*)", elt)):
                continue

        characteristics['number'] = elt.split(' ')[0]
        characteristics['raw_line'] = elt

        stock.append(characteristics)

    # Build list of potential headers numbers
    l_number = [e['number'] for e in stock]

    # Skip part of the potential headers numbers related to TOC
    stock_without_toc = stock.copy()
    if has_table_of_content: 
        count_first_header_element = l_number.count(l_number[0])

        if count_first_header_element > 1:
            first_header_element_index = [i for i, val in enumerate(l_number) if val==l_number[0]]
            stock_without_toc = stock_without_toc[first_header_element_index[1]:]

    # Remove not reliable elements
    stock_without_toc_cleaned = []
    for i, elt in enumerate(stock_without_toc):

        if len(stock_without_toc_cleaned) > 1 and i > 0:
            current_number_rank_1  = int(elt['number'].split('.')[0])
            previous_number_rank_1 = int(stock_without_toc_cleaned[-1]['number'].split('.')[0])

            # If not logical serie, we skip element
            if current_number_rank_1 < previous_number_rank_1:
                continue
            if current_number_rank_1 > previous_number_rank_1 + 1:
                continue

        stock_without_toc_cleaned.append(elt)

    # for elt in stock_without_toc_cleaned:
    #     print(f"[{elt['number']}] | deepness = {elt['deepness']} | {elt['raw_line']}")

    print('----------------------------------------------------------')

    # Find parent index on `deepness` condition
    tmp = []
    for i, elt in enumerate(stock_without_toc_cleaned):

        tmp.append(elt['deepness'])
        stock_without_toc_cleaned[i]['parent'] = len(tmp) - 1 - tmp[::-1].index(max(elt['deepness']-1,1))

    # Build Anytree Tree (https://anytree.readthedocs.io/en/latest/)
    tree_root = Node(f'TABLE OF CONTENT ({filepath})')
    for i, elt in enumerate(stock_without_toc_cleaned):

        stock_without_toc_cleaned[i]['tree'] = None
        if elt['deepness'] == 1:
            stock_without_toc_cleaned[i]['tree'] = Node(stock_without_toc_cleaned[i]['raw_line'], parent = tree_root)
        else:
            stock_without_toc_cleaned[i]['tree'] = Node(elt['raw_line'], parent = stock_without_toc_cleaned[stock_without_toc_cleaned[i]['parent']]['tree'])

    print('----------------------------------------------------------')

    # Print Anytree Tree
    for pre, fill, node in RenderTree(tree_root):
        print("%s%s" % (pre, node.name))

    ### Liste des CCTP ayant des problèmes de "retours chariots" > Analyser la variance de la longueur des lignes de chaque document
    # CCTP_menuiserie_2.txt
    # CCTP_menuiserie_6.txt
    # CCTP_menuiserie_15.txt
    # CCTP_menuiserie_22.txt (vide)
    # CCTP_menuiserie_28.txt
    # CCTP_menuiserie_32.txt
    # CCTP_menuiserie_50.txt
    # CCTP_menuiserie_58.txt
    # CCTP_menuiserie_62.txt (encoding)
    # CCTP_menuiserie_74.txt (vide)
    # CCTP_menuiserie_75.txt
    # CCTP_menuiserie_77.txt
    # CCTP_menuiserie_78.txt (vide)